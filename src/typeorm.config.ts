import { DataSource, DataSourceOptions } from 'typeorm';
import { config } from 'dotenv';
import { ConfigService } from '@nestjs/config';
import { Zus } from 'src/zus/entities/zus.entity';

config();

const configService = new ConfigService();

const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: configService.get('MYSQL_USER'),
  password: configService.get('MYSQL_PASS'),
  database: configService.get('MYSQL_DB'),
  logging: false,
  entities: [Zus],
  migrationsTableName: 'migrations',
  migrations: ['dist/migration/*.js'],
  subscribers: ['dist/subscribers'],
};

const dataSource = new DataSource(dataSourceOptions);

export default dataSource;