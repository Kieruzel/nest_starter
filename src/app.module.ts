import { Module, forwardRef } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ZusModule } from './zus/zus.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Zus } from 'src/zus/entities/zus.entity';

@Module({
  imports: [
    ZusModule,
    forwardRef(() =>
      TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: configService.get('MYSQL_USER'),
          password: configService.get('MYSQL_PASS'),
          database: configService.get('MYSQL_DB'),
          logging: false,
          entities: [Zus],
          migrationsTableName: 'migrations',
          migrations: ['dist/migration/*.js'],
          subscribers: ['dist/subscribers'],
        }),
        inject: [ConfigService],
      }),
    ),
    forwardRef(() =>
      ConfigModule.forRoot({
        isGlobal: true,
      }),
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
