import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ZusService } from './zus.service';
import { CreateZusDto } from './dto/create-zus.dto';
import { UpdateZusDto } from './dto/update-zus.dto';

@Controller('zus')
export class ZusController {
  constructor(private readonly zusService: ZusService) {}

  @Post()
  create(@Body() createZusDto: CreateZusDto) {
    return this.zusService.create(createZusDto);
  }

  @Get()
  findAll() {
    return this.zusService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.zusService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateZusDto: UpdateZusDto) {
    return this.zusService.update(+id, updateZusDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.zusService.remove(+id);
  }
}
